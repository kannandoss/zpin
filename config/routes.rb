Zpin::Application.routes.draw do
  root "pages#home"    
  get "home", to: "pages#home", as: "home"
  get "inside", to: "pages#inside", as: "inside"
  get "/contact", to: "pages#contact", as: "contact"
  post "/emailconfirmation", to: "pages#email", as: "email_confirmation"
  
  get "pins", to: "pins#index", as: "pins"
  post "pins/create"
  get "pins/pins"
  post "pins/favourite"
      
  devise_for :users
  
  namespace :admin do
    root "base#index"
    resources :users    
  end
  
end
