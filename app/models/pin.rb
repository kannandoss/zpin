class Pin < ActiveRecord::Base
	has_attached_file :pin, :styles => {:medium => "300x300"}
	validates_attachment_content_type :pin, :content_type => /\Aimage\/.*\Z/

	belongs_to :user
end
