module ApplicationHelper
  def title(value)
    unless value.nil?
      @title = "#{value} | Zpin"      
    end
  end
end
