class PinsController < ApplicationController
	def index
	end

	def create
		pin = Pin.new
		pin.name = params[:name]
		pin.description = params[:description]
		pin.pin = params[:pin]
		pin.user = current_user
		if pin.save
			flash[:notice] = "Successfully added a pin."
			redirect_to action: :index
		else
			flash[:error] = pin.full_error_messages.join(',')
			redirect_to inside_path
		end
	end

	def pins
		user_fav = current_user.favourites.collect{ |f| f.pin_id }
		pins = []
		Pin.all.each do |pin|
			p = {}
			p[:id] = pin.id
			p[:name] = pin.name
			p[:description] = pin.description
			p[:url] = pin.pin.url
			p[:murl] = pin.pin.url(:medium)
			p[:favourite] = user_fav.include?(pin.id)
			p[:owner] = (current_user.id == pin.user_id)
			pins << p
		end
		respond_to do |format|
			format.json { render :json => pins }
		end
	end

	def favourite
		if params[:make] == "fav"
			fav = Favourite.new
			fav.user = current_user
			fav.pin = Pin.find params[:id]
			if fav.save
				respond_to do |format|
					format.json { render :json => "Success"}
				end
			end
		elsif params[:make] == "unfav"
			fav = current_user.favourites.where(pin_id: params[:id]).first
			if fav.destroy
				respond_to do |format|
					format.json { render :json => "Success"}
				end
			end
		end
	end
end
