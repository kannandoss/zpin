# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#= require_self
#= require_tree ./Controllers/main

Pin = angular.module('Pin', ['ngRoute'])

Pin.config(['$routeProvider', ($routeProvider) ->
	$routeProvider.otherwise({templateUrl: '../assets/mainIndex.html', controller: 'IndexCtrl'})
])

Pin.config(['$httpProvider', (provider) ->
	provider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')
])