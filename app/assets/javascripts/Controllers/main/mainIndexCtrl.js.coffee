@IndexCtrl = ($scope, $http) ->
	$scope.data = "test..."

	$http.get('./pins/pins.json').success( (data) ->
		$scope.data = data
		console.log(data)
		console.log('Successfully loaded pins.')
	).error( ->
		console.log('Failed loading pins.')
	)

	$scope.makeFavourite = (pinId) ->
		data =
			id: pinId
			make: "fav"
		$http.post('./pins/favourite.json', data).success( (data) ->
			pin = _.findWhere($scope.data, {id: parseInt(pinId)})
			pin.favourite = true
			console.log('Successfully favourite.')
		).error( ->
			console.log('Failed to favourite.')
		)
		return

	$scope.makeUnfavourite = (pinId) ->
		data =
			id: pinId
			make: "unfav"
		$http.post('./pins/favourite.json', data).success( (data) ->
			pin = _.findWhere($scope.data, {id: parseInt(pinId)})
			pin.favourite = false
			console.log('Successfully unfavourite.')
		).error( ->
			console.log('Failed to unfavourite.')
		)
		return