class AddNameDescriptionToPins < ActiveRecord::Migration
  def change
  	add_column :pins, :name, :string
  	add_column :pins, :description, :text
  end
end
