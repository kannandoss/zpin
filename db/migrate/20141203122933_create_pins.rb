class CreatePins < ActiveRecord::Migration
  def change
    create_table :pins do |t|
      t.integer :user_id
      t.attachment :pin

      t.timestamps
    end
  end
end
